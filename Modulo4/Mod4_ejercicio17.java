package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 17
 * Suma de los valores pares de una tabla de multiplicar 
*/
public class Mod4_ejercicio17 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int suma = 0;
		System.out.println("Introduzca un numero entero: ");
		int n = input.nextInt();
		System.out.println("La suma de los valores pares de la tabla de multiplicar del "+n);
		for(int i=0; i<=10; i++) {
			if((n*i)%2==0) {
				suma +=n*i;
				//System.out.println(n*i+" ");
			}
		}
		System.out.println(suma);
	}

}
