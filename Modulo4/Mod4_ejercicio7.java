package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 7
 * Realizar un programa que permita la definición de 3 variables de tipo entera e imprimir la 
 * mayor de todas.
 * */
public class Mod4_ejercicio7 {

	public static void main(String[] args) {
		int v1, v2, v3;
		int mayor;
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce 3 variables enteras: ");
		v1 = input.nextInt();
		v2 = input.nextInt();
		v3 = input.nextInt();
		
		if((v1<v2)&&(v3<v2))	mayor = v2;				//variable 2 es la mayor
		else if((v2<v1)&&(v3<v1))	mayor = v1;			//variable 1 es la mayor
		else 	mayor = v3;								//variable 3 es la mayor
		
		System.out.println("El mayor es: "+mayor);
	}

}
