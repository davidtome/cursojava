package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 12
 * Realizar un sistema que permita asignar un numero dentro de una variable de tipo int y
 * sabiendo que 1-12 primera docena, 13-24 2a, y 25-36 3ra
*/
public class Modulo4_ejercicio12 {

	public static void main(String[] args) {
		int num =0;
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca un numero: ");
		num = input.nextInt();
		if((num<0)||(num>36)) System.out.println("El numero "+num+" esta fuera de rango.");
		else if(num<13)		System.out.println(num+" pertenece a la primera docena.");
		else if(num<25)		System.out.println(num+" pertenece a la segunda docena.");
		else 		System.out.println(num+" pertenece a la tercera docena.");
		

	}

}
