package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 15
 * realizar un sistema que permita mostrar las caracter�sticas de un auto utilizando para ello una
 * variable de tipo char, esta se llenara con los valores �a�, �b� o �c�, siendo de clase 
 * �a� los que tienen 4 ruedas y un motor, 
 * clase �b� 4 ruedas, un motor, cierre centralizado y aire, 
 * clase �c� 4 ruedas, un motor, cierre centralizado, aire, airbag.

*/
public class Mod4_ejercicio15 {

	public static void main(String[] args) {
		char tipo='f';
		Scanner input = new Scanner(System.in);
		do {
			System.out.println("Introduzca la letra de clase del coche: ");
			tipo = input.next().charAt(0);
		}while((tipo!='a')&&(tipo!='b')&&(tipo!='c'));
		switch(tipo){
			case 'a':
				System.out.println("Clase 'a' los que tienen 4 ruedas y un motor");
				break;
			case 'b':
				System.out.println("Clase 'b' los que tienen 4 ruedas, un motor, cierre centralizado y aire");
				break;
			case 'c':
				System.out.println("Clase 'c' los que tienen 4 ruedas, un motor, cierre centralizado, aire, airbag");
				break;
		}

	}

}
