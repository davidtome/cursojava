package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 6
 * Realizar un sistema que permita determinar a trav�s de una variable de tipo int correspondiente
 * al curso al que pertenece seg�n el siguiente criterio
 * */
public class Mod4_ejercicio6 {

	public static void main(String[] args) {
		int edad=15;
		Scanner input = new Scanner(System.in);
		do {
			System.out.println("Introduzca una edad (0-12): ");
			edad = input.nextInt();
		}while((edad<0)||(edad>12));
		if(edad==0) System.out.println("jard�n de infantes");
		else if(edad<=6) System.out.println("primaria");
		else System.out.println("secundaria");

	}

}
