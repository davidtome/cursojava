package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 8
 * Realizar un programa que permita tener dos variables de tipo int, si se define 0 como piedra, 1
 * como papel y 2 como tijera, teniendo 2 competidores, determinar cual de ellos es el ganador.f
 * con and y or
*/

public class Mod4_ejercicio8 {

	public static void main(String[] args) {
		int jugador1, jugador2;
		//random da un # de 0 a 0.99 al multiplicar x 3 da uno entre 0 y 2 
		do {
			jugador1 = (int)(Math.random()*3);
			jugador2 = (int)(Math.random()*3);
			System.out.println(jugador1+ "   "+jugador2);
			if(jugador1==jugador2) 		System.out.println("EMPATE!");
		}while(jugador1==jugador2);
		//0 piedra, 1 papel, 2 tijera
		if(((jugador1==0)&&(jugador2==1))||(jugador1==2)&&(jugador2==0)||(jugador1==1)&&(jugador2==2)) System.out.println("Jugador2 Ganador");
		else System.out.println("Jugador1 Ganador");	//resultado restante 2-1 al no haber posibilidad de empate
	}

}
