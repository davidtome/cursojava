package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 16
 * Realizar un programa que permita mostrar en pantalla la tabla de multiplicar de un valor
 * ingresado a trav�s de una variable.
*/
public class Mod4_ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca un numero entero: ");
		int n = input.nextInt();
		System.out.println("La tabla de multiplicar del "+n);
		for(int i=0; i<=10; i++) {
			System.out.println("\n "+n+" x "+i+" = "+(n*i));
		}

	}

}
