package modulo4;
import java.util.Scanner;
/*
 * Practica 4 ej 21
 * Calcular el sueldo neto de un empleado basado en categoria, antiguedad y sueldo.
 * Se quiere determinar el sueldo neto sabiendo que si la antig�edad esta entre
 * 1 y 5 a�os se le aumentan un 5% al sueldo bruto. 
 * 6 y 10 a�os se le aumenta un 10% al sueldo bruto. Mas de 10 un 30% Y un plus por categor�a
 * A= 1000, B=2000, c=3000
*/
public class Mod4_ejercicio21 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca la categoria del empleado (A, B o C): ");
		char categoria = input.next().charAt(0);
		System.out.println("Introduzca la antiguedad en a�os: ");
		int ant = input.nextInt();
		System.out.println("Introduzca el sueldo: ");
		double sueldo = input.nextDouble();
		int sumaCategoria = 0;
		switch(categoria) {
			case 'A':
				sumaCategoria = 1000;
				break;
			case 'B':
				sumaCategoria = 2000;
				break;
			case 'C':
				sumaCategoria = 3000;
				break;
		}
		if(ant<=5)	System.out.println("El sueldo es "+sueldo+" mas 5% mas "+sumaCategoria+" = "+(sueldo*1.05+sumaCategoria));
		else if (ant<=10)	System.out.println("El sueldo es "+sueldo+" mas 10% mas "+sumaCategoria+" = "+(sueldo*1.1+sumaCategoria));
		else	System.out.println("El sueldo es "+sueldo+" mas 30% mas "+sumaCategoria+" = "+(sueldo*1.3+sumaCategoria));
	}

}
