package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 5
 * Realizar un programa que permita identificar con una variable de tipo entero el puesto que
 * ocupa un torneo, existen 3 posiciones que son premiadas
 * */
public class Mod4_ejercicio5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int posicion = 0;
		String categoria ="Siga participando";
		do {
			System.out.println("Ingrese su posicion en el torneo: ");
			posicion = input.nextInt();
		}while(posicion<0);
		switch(posicion) {
			case 1: 
				categoria = "El primero obtiene la medalla de oro.";
				break;
			case 2:
				categoria = "El segundo obtiene la medalla de plata";
				break;
			case 3:
				categoria = "Y el tercero obtiene la medalla de bronce";
				break;
		}
		System.out.println(categoria);
	}

}
