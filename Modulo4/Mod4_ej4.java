package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 4
 * Realizar un programa que permita ingresar 
 * una categor�a las cuales pueden ser �a�, �b� o �c�,
 * hijo, padres, abuelos 
 * */
public class Mod4_ej4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		char c = 's';
		String categoria ="";
		do {
			System.out.println("Ingrese una categor�a que puede ser 'a', 'b', o 'c': ");
			c = input.next().charAt(0);
		}while((c!='a')&&(c!='b')&&(c!='c'));
		switch(c) {
			case 'a': 
				categoria = "hijos";
				break;
			case 'b':
				categoria = "padres";
				break;
			case 'c':
				categoria = "abuelos";
				break;
		}
		System.out.println("La categoria es "+categoria);
	}

}
