package modulo4;

import java.util.Scanner;

/*
 * Practica 4 ej 2
 * Realizar un programa que permita identificar 
 * si un n�mero es par o impar, el mismo deber�
 * estar guardado en una variable de tipo int.
 * */
public class Mod4_ejercicio2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;
		do {
			System.out.println("Escriba un numero entero mayor que 0: ");
			num = input.nextInt();
		}while(num<=0);
		if(num%2==0)
			System.out.println("El numero "+num+" es par");
		else 
			System.out.println("El numero "+num+" es impar");
	}

}
