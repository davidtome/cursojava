package modulo4;
/*
 * Practica 4 ej 19
 * Realizar un ciclo while que muestre 10 n�meros al azar informando su suma y su promedio
*/
public class Mod4_ejercicio19 {
	
	public static void main(String[] args) {
		int suma = 0;
		for(int i=0, num = 0;i<10;i++) {
			num = (int)(Math.random()*100); // numero random de 0 a 99
			System.out.println(num);
			suma +=num;
		}
		System.out.println("El promedio es "+(suma/10));
	}

}
