package modulo4;
/*
 * Practica 4 ej 20
 * Realizar un ciclo do while que muestre 10 n�meros al azar (Math.random() )informando el
 * m�ximo y el m�nimo de ellos.
*/
public class Mod4_ejercicio20 {
	
	public static void main(String[] args) {
		int min = -1, max = -1;
		for(int i=0, num=0;i<10;i++) {
			num = (int)(Math.random()*100); // numero random de 0 a 99
			System.out.println(num);
			if(min==-1) min = num;
			else if(max==-1) {
				if(max<min) {
					max = min;
					min = num;
				}
			}else if(max<num)	max = num;
			else if(min>num) min = num;
			if(max<min) {
				max = min;
				min = num;
			}
		}
		
		System.out.println("El maximo es "+max+ " y el minimo es "+min);
	}

}