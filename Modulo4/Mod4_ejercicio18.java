package modulo4;
/*
 * Practica 4 ej 18
 * Realizar dos ciclos for anidados de manera de mostrar todas las tablas
*/
public class Mod4_ejercicio18 {

	public static void main(String[] args) {
		for(int i=1;i<=10;i++) {
			System.out.println("La tabla del "+i);
			for(int j=0;j<=10;j++) {
				System.out.println(i+" x "+j+" = "+(i*j));
			}
		}

	}

}
