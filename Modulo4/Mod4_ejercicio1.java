package modulo4;

import java.util.Scanner;

/*
 * Practica 4 ej 1
 * Realizar un programa que permita definir 3 variables correspondientes a las evaluaciones de un
 * alumno de una escuela, sobre el particular se deber� informar lo siguiente:
 * a)Aprobado si el promedio es mayor igual a 7
 * b)Reprobado si el promedio es menor a esta nota
 * */

public class Mod4_ejercicio1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		float n1, n2, n3;
		System.out.println("Introduzca 3 notas: ");
		n1 = input.nextFloat();
		n2 = input.nextFloat();
		n3 = input.nextFloat();
		
		float promedio = (n1+n2+n3)/3;
		if(promedio<7) System.out.println("Suspenso. Tu promedio es "+promedio+2f);
		else System.out.println("Felicidades to promedio es "+promedio+0.1f);
		

	}

}
