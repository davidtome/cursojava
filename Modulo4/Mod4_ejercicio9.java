package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 9
 * Realizar el ejercicio 8 utilizando la sentencia and para resolver el problema
*/
public class Mod4_ejercicio9 {

	public static void main(String[] args) {
		int jugador1, jugador2;
		//random da un # de 0 a 0.99 al multiplicar x 3 da uno entre 0 y 2 
		do {
			jugador1 = (int)(Math.random()*3);
			jugador2 = (int)(Math.random()*3);
			System.out.println(jugador1+ "   "+jugador2);
			if(jugador1==jugador2) 		System.out.println("EMPATE!");
		}while(jugador1==jugador2);
		//0 piedra, 1 papel, 2 tijera
		if((jugador1==0)&&(jugador2==1)) System.out.println("Jugador2 Ganador. Papel gana a piedra");
		else if((jugador1==0)&&(jugador2==2)) System.out.println("Jugador1 Ganador. Piedra gana a tijera");
		else if((jugador1==1)&&(jugador2==0)) System.out.println("Jugador1 Ganador. Papel gana a piedra");
		else if((jugador1==2)&&(jugador2==0)) System.out.println("Jugador2 Ganador. Piedra gana a tijera");
		else if((jugador1==1)&&(jugador2==2)) System.out.println("Jugador2 Ganador. Tijera gana a papel");
		else System.out.println("Jugador1 Ganador. Tijera gana a papel");	//resultado restante 2-1 al no haber posibilidad de empate
	}

}

