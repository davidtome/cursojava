package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 11
 * Introduce un caracter y determina si es vocal o no
*/
public class Mod4_ejercicio11 {

	public static void main(String[] args) {
		char c; 
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca un caracter: ");
		c = input.next().charAt(0);
		if((c=='a')||(c=='e')||(c=='i')||(c=='o')||(c=='u')) 	System.out.println("'"+c+"' es una vocal.");
		else 	System.out.println("'"+c+"' es una consonante.");
	}

}
