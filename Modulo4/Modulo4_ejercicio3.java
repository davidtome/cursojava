package modulo4;
import java.util.Scanner;

/*
 * Practica 4 ej 3
 * Realizar un programa que permita determinar 
 * la cantidad de d�as que tiene el mes informando
 * el nombre del mismo acompa�ado de la cantidad de 
 * d�as que le corresponden.
 * */
public class Modulo4_ejercicio3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduzca un mes del a�o: ");
		String mes = input.next();
		int dias = 0;
		switch(mes) {
		case "enero":
			dias = 31;
			break;
		case "febrero":
			dias = 28;
			break;
		case "marzo":
			dias = 31;
			break;
		case "abril":
			dias = 30;
			break;
		case "mayo":
			dias = 31;
			break;
		case "junio":
			dias = 30;
			break;
		case "julio":
			dias = 31;
			break;
		case "agosto":
			dias = 31;
			break;
		case "septiembre":
			dias = 30;
			break;
		case "octubre":
			dias = 31;
			break;
		case "noviembre":
			dias = 30;
			break;
		case "diciembre":
			dias = 31;
			break;	
		}
		if(dias==0)
			System.out.println("Mes erroneo intentelo otra vez con minusculas");
		else 
			System.out.println("El mes de "+mes+" tiene "+dias+" dias");

	}

}
