package practica9;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//import Practica;
//import Model;
//import ConnectionManager;
import es.com.manpower.notas.modelo.dao.ConnectionManager;

public class PracticaDao implements DAO {
	private Connection conexion;
	
	public void PracticaDAO() {}
	
	@Override
	public void agregar(Model pMod) throws ClassNotFoundException, SQLException { //, SQLException
		Practica practica= (Practica)pMod;
		
		//1- me tengo que conectar
		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();
		//2- statemente
		
		StringBuilder sql = new StringBuilder("INSERT INTO practicas (ALU_CODIGO,ALU_NOMBRE, ALU_OBSERVACIONES) VALUES");
		sql.append("(?,?)");
		
		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setFloat(2, practica.getNota());
		pstm.setInt(1, practica.getCodigo());
		pstm.setString(3, practica.getObservaciones());
		
		pstm.executeUpdate();				
		ConnectionManager.desConectar();


	}
	
	@Override
	public void modificar(Model pMod) throws ClassNotFoundException, SQLException { //TODO error en sqlexception
		Practica practica= (Practica)pMod;
		
		//1- me tengo que conectar
		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();
		//2- statemente
		
		StringBuilder sql = new StringBuilder("update practicas");
		sql.append(" set ALU_NOTA=? ,ALU_CODIGO=?, ALU_OBSERVACIONES");
		sql.append(" where alu_id=?"); //TODO falta cambiar a primary key de practicas
		
		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setFloat(1, practica.getNota());
		pstm.setInt(2, practica.getCodigo());
		pstm.setString(2, practica.getObservaciones());
		pstm.executeUpdate();				
		ConnectionManager.desConectar();
	}


	@Override
	public void eliminar(Model pMod) throws ClassNotFoundException { //, SQLException
		Practica practica= (Practica)pMod;
		
		//1- me tengo que conectar
				ConnectionManager.conectar();
				conexion = ConnectionManager.getConection();
				//2- statemente
				
				StringBuilder sql = new StringBuilder("delete from practicas");		
				sql.append(" where alu_id=?"); //TODO falta cambiar a primary key de practicas
				
				PreparedStatement pstm = conexion.prepareStatement(sql.toString());
				pstm.setInt(1, practica.getCodigo());
				
				pstm.executeUpdate();				
				ConnectionManager.desConectar();	

	}
	
	@Override
	public List<Model> leer(Model pMod) throws ClassNotFoundException {	//, SQLException
		Practica practica= (Practica)pMod;
		List<Model> practicas = new ArrayList<Model>();
		
		//1- me tengo que conectar
		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();
		//2- statemente
		
		StringBuilder sql = new StringBuilder("SELECT ALU_CODIGO, ALU_NOTA"); //TODO ver si el orden de codigo y nombre influye
		sql.append(" from practicas");
		//utilizar patron strategy
		if(practica.getCodigo()>0)
			sql.append(" where alu_id=?");	
		
		//Hacer el segundo testeo de lectura 
		StringBuilder sql2 = new StringBuilder("SELECT ALU_OBSERVACIONES"); 
		sql2.append(" from practicas");
		if(practica.getCodigo()>0)
			sql2.append(" where alu_id=?");	//en el caso de que id=3 existiese 
		
		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setInt(1, practica.getCodigo());
		
		ResultSet rs =pstm.executeQuery();				
		
		while(rs.next()){
			practicas.add(new Practica(	rs.getInt("ALU_CODIGO")			,
									rs.getFloat("ALU_NOTA")));		
		}
		rs.close();
		ConnectionManager.desConectar();
		
		return  practicas;
	}

}


