package practica9;

import static org.junit.Assert.*;

import java.beans.Statement;
import java.io.BufferedReader;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.jdi.connect.spi.Connection;
import com.sun.tools.javac.util.List;

import es.com.manpower.notas.modelo.dao.ConnectionManager;
import es.com.manpower.notas.modelo.dao.PracticaResuelta;

public class PracticaDaoTest {
	PracticaDao practicaDAO;

	@Before
	public void setUp() throws Exception {
		practicaDAO = new PracticaDao();
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( PracticaDAOTest.class.getResource( "PracticasCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desConectar();

	}

	@After
	public void tearDown() throws Exception {
		practicaDAO = null;
	}

	@Test
	public void testAgregar() {
		try {
			practicaDAO.agregar(new Practica(77, 7, "nueva nota anadida"));
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select alu_observaciones from practicas where pr_observaciones ='nueva nota anadida' ");
			rs.next();
			assertEquals("nueva nota anadida", rs.getString("alu_observaciones"));
			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}
	
	@Test
	public void testModificar() {
		try {
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select pr_observaciones from practicas where pr_observaciones ='nada'");
			rs.next();
			Practica p = new Practica(rs.getInt("alu_id"));
			practicaDAO.modificar(p);
			
			StringBuilder sql=new StringBuilder("select alu_codigo, alu_nota, alu_observaciones ");
			sql.append("from practicasresueltas ");
			sql.append("where pr_observaciones='modificao' ");
			
			rs=stm.executeQuery(sql.toString());
			// hago la segunda prueba de modificar con assert 
			
			Practica pLeida=new Practica(rs.getInt("pr_id"),
			rs.getInt("prac_id"), 
			rs.getInt("alu_id"), 
			rs.getInt("pr_nota"), 
			rs.getString("pr_observaciones"));

			assertEquals(p.getCodigo(), pLeida.getCodigo());
			assertEquals(p.getNota(), pLeida.getNota());
			assertEquals(p.getObservaciones(), pLeida.getObservaciones());


			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEliminar() {
		try {
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select alu_observaciones from practicas where alu_observaciones ='nada'");
			rs.next();

			// 2- elimino
			Practica p = new Practica();
			practicaDAO.eliminar(p);
			
			// 3- comprobamos que se ha eliminado
			rs = stm.executeQuery("Select  alu_observaciones  from practicas where alu_observaciones ='nada'");
			assertFalse(rs.next());

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer() {
		try {
			// 1- leer
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select  alu_observaciones  from practicas where alu_observaciones ='nada'");
			rs.next();

			Practica p = new Practica(rs.getInt("pr_id"));
			List<Model> practicasRes = (List<Model>) practicaDAO.leer(p);
			
			// comprobamos que se haya leido correctamente
			assertEquals("leer", ((PracticaResuelta) practicasRes.get(0)).getObservaciones());

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

}
