package practica9;

public class Practica implements Model {
	private int codigo;
	private float nota;
	private String observaciones;
	
	public Practica() {
		super();
		this.codigo = 0;
		this.nota = 0;
		this.observaciones = null;
	}

	public Practica(int codigo, float nota, String observaciones) {
		super();
		this.codigo = codigo;
		this.nota = nota;
		this.observaciones = observaciones;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean equals(Object obj){
		return obj instanceof Practica 										&&
				((Practica)obj).getNota()==nota							    &&
				((Practica)obj).getObservaciones().equals(observaciones)    &&
				((Practica)obj).codigo==codigo;
	}
	public int hashCode(){
		return  + codigo; //TODO comprobar que hash code de codigo se devuelve asi
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder("codigo=");
		sb.append(this.codigo);
		sb.append(", nota=");
		sb.append(this.nota);	
		return sb.toString();
	}
	
}



