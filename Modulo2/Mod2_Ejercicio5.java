package modulo2;

public class Mod2_Ejercicio5 {

	public static void main(String[] args) {
		// Modulo 234 ejercicio 5: Cual de las siguientes líneas dan errores de compilación y para esos casos cubrirlos con el casteo
		// correspondiente

		byte b=10;
		short s=20;
		int i = 30;
		long l= 40;
		l = s;
		System.out.println("long "+l+ " = short "+s);
		b=(byte)s;
		System.out.println("byte "+b+" = short "+s);
		l=i;
		System.out.println("long "+l+ "= int " +i);
		b=(byte)i;
		System.out.println("byte "+b+"= int " +i);
		s=(short)i;
		System.out.println("short ="+s+ " int " +i);
		
	}

}
