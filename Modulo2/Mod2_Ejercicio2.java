package modulo2;

public class Mod2_Ejercicio2 {

	public static void main(String[] args) {
		/* Modulo 2 ejercicio 2:  mostrar en
		pantalla todos los tipos de datos enteros con sus correspondientes m�ximos y m�nimos, para
		ello, una variable para cada uno de los casos como se muestra a continuaci�n.
		*/

		byte bmin = -128;
		byte bmax = 127;
		// reemplazar el 0 por el valor que corresponda en todos los caso
		short smin = (short) ((short) Math.pow(2, 15)*(-1));
		short smax = (short) (Math.pow(2, 15) - 1);
		int imax = (int) (Math.pow(2, 31)*(-1));
		int imin = (int) (Math.pow(2, 31) - 1);
		long lmin = (long) (Math.pow(2, 63)*(-1));
		long lmax = (long) (Math.pow(2, 63) - 1);;
		System.out.println("Tipos de dato\tminimo\t\t\tmaximo");
		System.out.println("--------------\t------\t\t\t------");
		System.out.println("\nbyte\t\t" + bmin + "\t\t\t" + bmax);
		System.out.println("\nshort\t\t" + smin + "\t\t\t" + smax);
		System.out.println("\nint\t\t" + imin + "\t\t" + imax);
		System.out.println("\nlong\t\t" + lmin + "\t" + lmax);
	}

}
