package modulo2;

public class Mod2_Ejercicio3 {

	public static void main(String[] args) {
		/* Modulo234 ejercicio 3:  realizar un programa que permita alojar en variables la siguiente informaci�n
		utilizando el tipo de dato que ocupe la menor cantidad de memoria posible.
		*/
		//Tipo de divisi�n �a�,�b� o �c�.
		char division = 'a';
		//Cantidad de goles por partido
		byte goles = 81;
		//La capacidad de la cancha por ejemplo river (70.000 personas)
		int capacidadRiver = 70000;
		//Promedio de goles
		float promedio = (float)(3+4)/2;

		System.out.println("Division de River "+division+ " goles " +goles+ " capacidad del estadio "
				+capacidadRiver+ " promedio de goles " +promedio);
	}

}
