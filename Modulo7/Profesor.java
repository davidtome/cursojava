package practica7;

import java.util.Objects;

public class Profesor extends Persona {
		//atributos
		private String iosfa; //numero de seguridad social
		
		//constructores
		public Profesor() {		
			super();
			iosfa = "null";;
		}

		public Profesor(String name, String last, String ssn) {
			super(name, last);
			iosfa = ssn;		
		}

		//accessors
		public String getIosfa() {					return iosfa;			}
		public void setIosfa(String ssn) {		this.iosfa = ssn;	}
		
		public boolean equals(Object obj){		
			return obj instanceof Profesor 					&&
					super.equals(obj)  							&&
					iosfa == ((Profesor)obj).getIosfa();		
		}
		public int hashCode(){
			return super.hashCode() + Objects.hash(iosfa);
		}
		public String toString(){
			return "\nProfesor " +super.toString() + ",iosfa=" + iosfa;
		}

	}
