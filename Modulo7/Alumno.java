package practica7;

public class Alumno extends Persona {
	//atributos
	private int legajo; //numero de expediente
	
	//constructores
	public Alumno() {		
		super();
		legajo = 10;
	}

	public Alumno(String name, String last, int lega) {
		super(name, last);
		legajo=lega;		
	}

	//accessors
	public int getLegajo() {					return legajo;			}
	public void setLegajo(int legajo) {		this.legajo = legajo;	}
	
	public boolean equals(Object obj){		
		return obj instanceof Alumno 					&&
				super.equals(obj)  							&&
				legajo == ((Alumno)obj).getLegajo();		
	}
	public int hashCode(){
		return super.hashCode() + legajo;
	}
	public String toString(){
		return "\nAlumno " +super.toString() + ",legajo=" + legajo;
	}

}
