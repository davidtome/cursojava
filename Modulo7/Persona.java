package practica7;

import java.util.Objects;

/**
 * @author David Tom�
 * Practica 7 clase Persona principal
 */
public abstract class Persona {
	private String nombre;
	private String apellido;
	
	//constructores
	public Persona(){
		nombre = "David";
		apellido = "Tome";
	}
	public Persona(String name, String last){
		nombre = name;
		apellido  = last;
	}
	
	public void setNombre(String name){		nombre = name;	}
	public void setApellido(String last){		nombre = last;	}
	
	public String getNombre(){					return nombre;		}
	public String getApellido(){					return apellido;		}
	
	public boolean equals(Object obj){
		//establezco las reglas de negocio como yo quiero 
		
		boolean bln =false;
		if(obj instanceof Persona){
			//downcast			
			Persona per = (Persona) obj;
			bln = 	this.nombre == per.getNombre() &&
					this.apellido 	== per.getApellido();
		}
		return bln;
	}
	public int hashCode(){
		return nombre.hashCode() + apellido.hashCode();
		//return Objects.hash(nombre, apellido);
	}
	public String toString(){
		return "Nombre: " + nombre + ", apellido: " + apellido;
	}
}