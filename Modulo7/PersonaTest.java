package practica7;

public class PersonaTest {
	public static void main(String[] args) {
		//crea un objeto
		Persona p1 = new Alumno();
		Persona p2 = new Alumno("Pablo", "Garcia", 997862);
		
		System.out.println("p1=" + p1);
		System.out.println("p2=" + p2);
		//probando que el contructor funciona
		//c1 = null;
		System.out.println("se creo el alumno 1 de nombre=" + p1.getNombre());
		System.out.println("su apellido es " + p1.getApellido()); 
	
		System.out.println("\nse creo el alumno 2 de nombre=" + p2.getNombre());
		System.out.println("su apellido es " + p2.getApellido());
	
	}

}

