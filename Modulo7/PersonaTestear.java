package practica7;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetos.CajaDeAhorro;
import objetos.Cuenta;
import objetos.CuentaCorriente;

public class PersonaTestear {
    Cuenta cajaDeAhorraVacio;
    Cuenta cajaDeAhorroLleno;
    Cuenta cc1Vacio;
    //lisy y set son interfaces
    List<Cuenta> lstCuentas; //pemite duplicados y es ordered (uno atras del otro
    Set<Cuenta> setCuentas; //NO PERMITE suplicado y se guardan solo DIOS sabe;

	@Before
	public void setUp() throws Exception {
		//upcast 
			
		cajaDeAhorraVacio = new Alumno();
		cajaDeAhorroLleno = new Alumno("Pedro","Gil", 78326);
		cc1Vacio = new CuentaCorriente();
		
		lstCuentas = new ArrayList<Persona>();
		lstCuentas.add(new Alumno());
		lstCuentas.add(new Profesor());
		
		//cajadeahorro alumno
		//cuentacorriente profesor
		
		lstCuentas.add(new Alumno("Pedro","Gil", 783));
		lstCuentas.add(new Alumno("Paula","Gil", 732);
		lstCuentas.add(new Alumno("Jose","Gil", 7826));
		
		lstCuentas.add(new Profesor("Gerard","Gomes", 56484215L));
		lstCuentas.add(new Profesor("Guti","Gomes", 56447215R));
		lstCuentas.add(new Profesor("Mario","Gomes", 654684215P));

		setPersonas = new HashSet<Persona>();
		setPersonas.add(new Alumno());
		setPersonas.add(new Profesor());
		
		setPersonas.add(new Alumno(1,10,0.1f));
		setPersonas.add(new Alumno(1,10,0.1f));
		setPersonas.add(new Alumno(1,10,0.1f));
		
		setPersonas.add(new Profesor("Paula","Perez", 96384215L));
		setPersonas.add(new Profesor("Vero","Ji", 87984215L));
		setPersonas.add(new Profesor("Oscar","Ito", 12484215L));
		
	}

	@After
	public void tearDown() throws Exception {
		alumnoVacio = null;
		alumnoLleno = null;
		cc1Vacio	= null;
		lstPersonas  = null;
		setPersonas  = null;
		
	}
	@Test
	public void listaEqualsContainsTRUE(){
		Persona cPrueba = new Alumno();
		assertTrue(lstPersonas.contains(cPrueba));
	}
	@Test
	public void listaEqualsContainsFALSE(){
		Persona cPrueba = new Alumno();
		assertFalse(lstPersonas.contains(cPrueba));
	}

	@Test
	public void testAlumnoEqualsVERDADERO(){
		Persona alumno = new Alumno();
		assertTrue(alumnoVacio.equals(alumno));
	}
	
	@Test
	public void testAlumnoEqualsFALSO(){
		Cuenta alumno = new Alumno();
		assertFalse(alumnoVacio.equals(alumno));
	}
	
	@Test
	public void testPersona() {
		//pruebo el contrtructor vacio
		assertEquals(10, cajaDeAhorraVacio.getNumero());
		assertEquals(1000.0f, cajaDeAhorraVacio.getSaldo(), 0.01);
		
	}

	@Test
	public void testCuentaIntFloat() {
		assertEquals(20, cajaDeAhorroLleno.getNumero());
		assertEquals(2000.0f, cajaDeAhorroLleno.getSaldo(), 0.01);
	}

	@Test
	public void testAcreditar() {
		cajaDeAhorraVacio.acreditar(100.53f);
		assertEquals(1100.53f, cajaDeAhorraVacio.getSaldo(), 0.01);
	}

	@Test
	public void testDebitar() {
		cajaDeAhorraVacio.debitar(100);
		assertEquals(900.0f, cajaDeAhorraVacio.getSaldo(),0.01);
	}
	@Test
	public void testDebitaAlcanzaEncc(){
		//tengo saldo de 1000
		//descubierto 150
		cc1Vacio.debitar(200);
		assertEquals(800.0f, cc1Vacio.getSaldo(), 0.01);
	}
	//TODO a los alumnos osados que agreguen los test que crean neceario hasta que pinche

}
