package patrones.factory.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PiedraPapelTijeraFactoryTest {
	PiedraPapelTijeraFactory pi, t, pa;
	
	@Before
	public void setUp() throws Exception {
		pi = new Piedra();
		pa = new Papel();
		t = new Tijera();
		// comparar tijera con piedra(0); tijera gana por lo tanto resultado deberia ser 1
		assertTrue(t.comparar(PiedraPapelTijeraFactory.getInstance(0))==1);
		// comparar piedra con papel(1); piedra pierde por lo tanto resultado deberia ser -1
		assertTrue(pi.comparar(PiedraPapelTijeraFactory.getInstance(1))==-1);
		// comparar papel con piedra(0); empate resultado deberia ser 0
		assertTrue(pa.comparar(PiedraPapelTijeraFactory.getInstance(0))==0);
		//TODO tambien se podria hacer con assertEquals
		
		assertTrue(PiedraPapelTijeraFactory.getInstance(0) instanceof Piedra);
		assertTrue(PiedraPapelTijeraFactory.getInstance(1) instanceof Papel);
		assertTrue(PiedraPapelTijeraFactory.getInstance(2) instanceof Tijera);
	}

	//@AfterEach
	public void tearDown() throws Exception {
		pi=null;
		t=null;
		pa=null;

	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
