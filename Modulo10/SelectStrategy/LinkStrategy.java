package es.com.manpower.notas.modelo.dao.strateg;

public class LinkStrategy extends SelectStrategy{

	public LinkStrategy() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder("");
		if(tengoWhere) {
			sb.append(" and alu_linkgit='");
		}
		else {
			sb.append(" where alu_linkgit='");
			tengoWhere=true;
		}
			
		sb.append(alumno.getLinkArepositorio());
		sb.append("'");
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		// TODO Auto-generated method stub
		return false;
	}

}
