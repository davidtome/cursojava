package es.com.manpower.notas.modelo.dao.strateg;

public class ApellidoStrategy extends SelectStrategy {

	public ApellidoStrategy() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder("");
		if(tengoWhere) {
			sb.append(" and alu_apellido=");
		}
		else {
			sb.append(" where alu_apellido=");
			tengoWhere=true;
		}
			
		sb.append(alumno.getApellido());
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		return (alumno.getApellido()!=null && !alumno.getApellido().isEmpty());
	}

}
