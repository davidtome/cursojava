package es.com.manpower.notas.modelo.dao.strateg;

public class NombreStrategt extends SelectStrategy {

	public NombreStrategt() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder("");
		if(tengoWhere) {
			sb.append(" and alu_nombre=");
		}
		else {
			sb.append(" where alu_nombre=");
			tengoWhere=true;
		}
			
		sb.append(alumno.getNombre());
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		return alumno.getNombre()!=null && !alumno.getNombre().isEmpty();
	}

}
