package practica8;

import java.util.Objects;

public class Circulo extends Figura{
	private float radio;

	public Circulo() {
		super();
		radio = 0;
		
	}
	
	public Circulo(String nombre, float radio) {
		super(nombre);
		this.radio = radio;
	}
	
	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}
	
	public float cacularPerimetro() {
		return (float)(2*Math.PI*radio);
	}
	
	public float calcularSuperficie() {
		return (float)(2*Math.PI*Math.pow(radio, 2));
	}
	
	public String getValores() {
		return "radio="+radio;
	}
	
	public boolean equals(Object c) {
		return c instanceof Circulo 			&&
				super.equals(c)					&&
				radio==((Circulo)c).radio;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(radio);
		return result;
	}
	
	public String toString() {
		return "Circulo: radio="+radio;
	}
}
