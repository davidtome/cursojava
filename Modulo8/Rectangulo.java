package practica8;

import java.util.Objects;

public class Rectangulo extends Figura{
	private float base;
	private float altura;

	public Rectangulo() {
		super();
		base = 0;
		altura = 0;
	}
	
	public Rectangulo(String nombre) {
		super(nombre);
		base = 0;
		altura = 0;
	}
	
	public Rectangulo(String nombre, float altura, float base) {
		super(nombre);
		this.base = base;
		this.altura = altura;
	}
	
	public float getAltura() {
		return altura;
	}

	public float getBase() {
		return base;
	}
	
	public void setAltura(float altura) {
		this.altura = altura;
	}
	
	public void setBase(float base) {
		this.base = base;
	}
	
	public float cacularPerimetro() {
		return 2*altura + 2*base;
	}
	
	public float calcularSuperficie() {
		return base*altura;
	}
	
	public boolean equals(Object c) {
		return c instanceof Rectangulo 			&&
				super.equals(c)					&&
				base==((Rectangulo)c).base		&& 
				altura==((Rectangulo)c).altura;

	}
	
	public String getValores() {
		return "base="+base+" altura="+altura;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(altura, base);
		return result;
	}
	
	public String toString() {
		return "Rectangulo: base="+base+" altura="+altura;
	}
}
