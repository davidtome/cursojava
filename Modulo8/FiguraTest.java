package practica8;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FiguraTest {
	Figura cir, cua, rec, tri, cirVacio, cuaVacio, recVacio, triVacio;
	Set<Figura> setFiguras;
	List<Figura> listaFiguras;

	@Before
	public void setUp() throws Exception {
		cir=new Circulo("circulo", 4);
		cua=new Cuadrado("cuadrado", 4);
		rec=new Rectangulo("rectangulo", 2, 4);
		tri=new Triangulo("triangulo", 3, 4, 5);
		cirVacio=new Circulo();
		cuaVacio=new Cuadrado();
		recVacio=new Rectangulo();
		triVacio=new Triangulo();

		setFiguras=new HashSet<Figura>();
		setFiguras.add(new Circulo());
		setFiguras.add(new Cuadrado());
		
		listaFiguras=new ArrayList<Figura>();
		listaFiguras.add(new Triangulo());
		listaFiguras.add(new Rectangulo());

	}

	@AfterEach
	public void tearDown() throws Exception {
		cir=null;
		cua=null;
		rec=null;
		tri=null;
		cirVacio=null;
		cuaVacio=null;
		recVacio=null;
		triVacio=null;
		
		setFiguras=null;
		listaFiguras=null;

	}

	@Test
	public void test() {
		assertTrue(cir.calcularPerimetro()==(float)(2*Math.PI*4));
		assertTrue(cua.calcularPerimetro()==(float)(4*4));
		assertTrue(rec.calcularSuperficie()==(float)(2*4));
		assertTrue(tri.calcularPerimetro()==12);
		
		Figura cirV=new Circulo();
		Figura cuaV=new Cuadrado();
		Figura recV=new Rectangulo();
		Figura triV=new Triangulo();
		
		assertEquals(cuaV, cuaVacio);
		assertEquals(cuaV, cuaVacio);
		assertEquals(cuaV, cuaVacio);
		assertEquals(cuaV, cuaVacio);
		
	}

}
