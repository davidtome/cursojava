package practica8;

import java.util.Objects;

public class Triangulo extends Figura{
	private float lado1;
	private float lado2;
	private float lado3;

	public Triangulo() {
		super();
		lado1 = 0;
		lado2 = 0;
		lado3 = 0;
	}
	
	public Triangulo(String nombre) {
		super(nombre);
		lado1 = 0;
		lado2 = 0;
		lado3 = 0;
	}
	
	public Triangulo(String nombre, float l1, float l2, float l3) {
		super(nombre);
		lado1 = l1;
		lado2 = l2;
		lado3 = l3;
	}
	
	public float getLado1() {
		return lado1;
	}

	public float getLado2() {
		return lado2;
	}
	
	public float getLado3() {
		return lado3;
	}
	
	public void setLado1(float lado) {
		this.lado1 = lado;
	}
	
	public void setLado2(float lado) {
		this.lado2 = lado;
	}
	
	public void setLado3(float lado) {
		this.lado3 = lado;
	}
	
	public float cacularPerimetro() {
		return lado1 + lado2 + lado3;
	}
	//altura = c^2-b^2
	// area = base*altura/2
	public float calcularSuperficie() {
		float b = lado2/2;
		float altura = (float) Math.sqrt((Math.pow(lado1, 2)-Math.pow(b, 2)));
		return (altura*b)/2;
	}
	
	public boolean equals(Object c) {
		return c instanceof Triangulo 				&&
				super.equals(c) 	  				&&
				((Triangulo)c).getLado1() == lado1 	&&
				((Triangulo)c).getLado2() == lado1 	&&
				((Triangulo)c).getLado3() == lado1;
	}
	
	public String getValores() {
		return "lado 1="+lado1+" lado 2="+lado2+" lado 3="+lado3;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(lado1, lado2, lado3);
		return result;

	}
	
	public String toString() {
		return "Triangulo: lado 1="+lado1+" lado 2="+lado2+" lado 3="+lado3;
	}
}
