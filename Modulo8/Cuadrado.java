package practica8;

import java.util.Objects;

public class Cuadrado extends Figura{
	private float lado;

	public Cuadrado() {
		super();
		lado = 0;
	}
	
	public Cuadrado(String nombre, float lado) {
		super(nombre);
		this.lado = lado;
	}
	
	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}
	
	public float cacularPerimetro() {
		return (lado+lado+lado+lado);
	}
	
	public float calcularSuperficie() {
		return 4*lado;
	}
	
	public String getValores() {
		return "lado="+lado;
	}
	
	public boolean equals(Object c) {
		return c instanceof Cuadrado 			&&
				super.equals(c)					&&
				lado==((Cuadrado)c).lado;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(lado);
		return result;
	}
	
	public String toString() {
		return "Cuadrado: lado="+lado;
	}
}