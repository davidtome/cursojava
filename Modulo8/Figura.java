package practica8;

import java.util.Objects;

public abstract class Figura {
	protected static float maximaSuperficie;
	private String nombre;
	
	public Figura() {}

	public Figura(String n) {	this.nombre = n;	}
	
	public static float getMaximaSuperficie() {
		return maximaSuperficie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public abstract float calcularPerimetro() {
		
	}
	
	public abstract float calcularSuperficie() {
		
	}
	
	public boolean equals(Object fig) {
		return fig instanceof Figura 			&&
				nombre==((Figura)fig).nombre	&&
				maximaSuperficie==((Figura)fig).maximaSuperficie;
	}
	
	public String getValores() {
		return "nombre="+nombre+" maxima superficie="+maximaSuperficie;
	}
	
	public int hashCode() {
		return Objects.hash(nombre);;
	}
	
	public String toString() {
		return "Figura: nombre="+nombre+" maxima superficie="+maximaSuperficie;
	}
}
