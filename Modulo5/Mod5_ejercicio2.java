package objetos;
/**
 * @author David Tom�
 * M�dulo 5 ejercicio 2
 * Realizar un programa que cree un objeto de tipo String con el texto 
 * �curso de Java� y muestre su contenido en varias l�neas, utilizar 
 * para ellos los m�todos charAt() y length()
 */
public class Mod5_ejercicio2 {

	public static final String texto = "curso de Java";
	public static void main(String[] args) {
		int lText = texto.length(); //Exception indexOutOfBounds, tama�o es 1 mayor al real
		for(int i=0; i<lText; i++) {
			// el ejercicio escribe una palabra por linea
			if(texto.charAt(i)==' ') 
				System.out.println();
			else
				System.out.print(texto.charAt(i));
		}
	}

}
