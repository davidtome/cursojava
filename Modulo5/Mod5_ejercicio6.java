package objetos;
/**
 * @author David Tom�
 * M�dulo 5 ejercicio 6
 * Realizar un programa que permita identificar 
 * en una cadena de tipo String si contiene n�meros.
 */
public class Mod5_ejercicio6 {
	private final static String cadena1= "hhfhw.ie76gfge,fre9";
	private final static String cadena2= "hhfhw.hgi iuwe";
	
	public static void main(String[] args) {
		int cons = 0, vocal = 0, signo = 0;
		boolean num1 = false, num2 = false;
		for(int i=0;(i<cadena1.length())&&(num1==false);i++) {
			num1 = esNum(cadena1.charAt(i));
		}
		for(int i=0;(i<cadena2.length())&&(num2==false);i++) {
			num2 = esNum(cadena2.charAt(i));
		}
		if(num1==true) 	System.out.println("La cadena 1 contiene numeros.");
		else System.out.println("La cadena 1 NO contiene numeros.");
		if(num2==true) 	System.out.println("La cadena 2 contiene numeros.");
		else System.out.println("La cadena 1 NO contiene numeros.");
	}
	// funcion de tipo boolean que detecta si un char pasado es numero
	public static boolean esNum(char c) {
		if((c=='1')||(c=='2')||(c=='3')||(c=='4')||(c=='5')||
				(c=='6')||(c=='7')||(c=='8')||(c=='9'))	return true;
		return false;
	}
}
