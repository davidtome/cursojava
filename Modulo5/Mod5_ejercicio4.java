package objetos;
/**
 * @author David Tom�
 * M�dulo 5 ejercicio 4
 * Realizar un programa que permita crear un objeto de tipo String con el texto �esto es una
 * prueba de la clase String�, luego recorrerlo y determinar la 
 * cantidad de vocales y consonantes que tiene el texto.
 */
public class Mod5_ejercicio4 {

	private final static String frase= "esto es una prueba de la clase String";
	public static void main(String[] args) {
		int cons = 0, vocal = 0, signo = 0;
		for(int i=0;i<frase.length();i++) {
			if(esVocal(frase.charAt(i)))	vocal++;
			else if(esSigno(frase.charAt(i)))	signo++;
			else 	cons++;
		}
		System.out.println(frase+"\nTiene:\nVocales: "+vocal+"\nConsonantes: "+cons);
	}
	// funcion de tipo boolean que detecta si un char pasado es vocal
	public static boolean esVocal(char c) {
		if((c=='a')||(c=='e')||(c=='i')||(c=='o')||(c=='u')||
				(c=='A')||(c=='E')||(c=='I')||(c=='O')||(c=='U'))	return true;
		return false;
	}
	// funcion de tipo boolean que detecta si un char pasado es signo de puntuacion
		public static boolean esSigno(char c) {
			if((c=='.')||(c==':')||(c==',')||(c==';')||(c==' '))	return true;
			return false;
		}

}
