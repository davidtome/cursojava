package objetos;
/**
 * @author David Tom�
 * M�dulo 5 ejercicio 5
 * Realizar un programa que permita crear un objeto de 
 * tipo String con el texto gcasas1972@gmail.com y realizar lo siguiente:
 * Determinar la posici�n de @ e informarla.
 * Obtener los strings gcasas1972 y gmail.com a trav�s del uso de sub cadenas 
 */
public class Mod5_ejercicio5 {
	private final static String email= "gcasas1972@gmail.com";
	public static void main(String[] args) {
		int pAt = -1;
		for(int i=0;(i<email.length())&&(pAt==-1);i++) {
			if(email.charAt(i)=='@')	pAt = i;
		}
		if(pAt==-1)	System.out.println("No se ha podido obtener '@'");
		else {
			System.out.println("La posicion de '@' es: "+pAt);
			System.out.println("Subcadena1: "+email.substring(0, pAt-1));
			System.out.println("Subcadena1: "+email.substring(pAt+1, email.length())); //pAt es @
		}

	}

}
