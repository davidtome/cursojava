package objetos;
/**
 * @author David Tom�
 * M�dulo 5 ejercicio 3
 * Realizar un programa que cree dos objetos de tipo String uno con el nombre y otro con el
 * apellido y en un tercer objeto realizar la suma de los dos con un espacio en el medio (recordar
 * que el objeto String es inmutable).

 */
public class Mod5_ejercicio3 {
	public static final String nombre = "Leo";
	public static final String apellido = "Messi";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fullName = nombre+" "+apellido;
		System.out.println("Nombre completo: "+fullName.toString());

	}

}
