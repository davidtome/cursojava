package objetos;

/**
 * @author David Tom�
 * M�dulo 5 ejercicio 1
 * Realizar un programa que cree un objeto String con el contenido �Hola mundo� y mostrar:
 * El texto todo en may�sculas
 * El texto todo en min�sculas
 * Reemplazar la letra �o� por el n�mero 2.
 */
public class Mod5_ejercicio1 {

	public static void main(String[] args) {
		String ini = "Hola mundo";
		System.out.println("1 - Frase en mayusculas: "+ini.toUpperCase());
		System.out.println("2 - Frase en minusculas: "+ini.toLowerCase());
		System.out.println("3 - Frase reemplazando 'o' por '2': "+ini.replace('o', '2'));
		

	}

}
