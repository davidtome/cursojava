package modulo6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author David Tom�
 * M�dulo 6 ejercicio 2
 * Realizar una clase DateUtil, con lo cual deber� estar familiarizado con el uso de 
 * Date, Calendar, y realizar los siguientes m�todos
 */

public class Mod6_ejercicio2 {
	//TODO las lineas siguientes, metodo main se ha creado para testear
/*	public static void main(String[] args) {
		Date fecha = new Date();
		System.out.println(fecha);
		System.out.println("Hoy es "+getDiaDeSemana(fecha)+" por lo tanto es fin de semana es "+isFinDeSemana(fecha));
		System.out.println("Estamos en "+getMes(fecha)+" en concreto "+asDate("yyy-mm-dd-EEE", "2032-11-11-6"));
		System.out.println("Estamos en la zona horaria y fecha: "+asCalendar("yyy-mm-dd-EEE", "2032-11-11-6").getTime());
		System.out.println("Estamos en "+asString("yyy-mm-dd-EEE", fecha));
	}*/
	public Mod6_ejercicio2() {}
	
	//returns year 
	public static int getAnio(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		return cal.get(Calendar.YEAR);
	}
	//returns month
	public static int getMes(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.MONTH);
	}
	//returns day
	public static int getDia(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	//returns true if its saturday or sunday
	public static boolean isFinDeSemana(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		int dWeek = cal.get(Calendar.DAY_OF_WEEK);
		if((dWeek==1)||(dWeek==7))	return true;
		return false;
	}
	//returns true if its a week day
	public static boolean isDiaDeSemana(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		int dWeek = cal.get(Calendar.DAY_OF_WEEK);
		if((dWeek!=1)&&(dWeek!=7))	return true;
		return false;
	}
	//returns the day of the week
	public static int getDiaDeSemana(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	//given a string 'pattern' sets the date d/m/y 
	//SimpleDateFormat sdf = new SimpleDateFormat("yyy-mm-dd-EEE");
	public static Date asDate(String pattern, String fecha) {
		SimpleDateFormat sdf = new SimpleDateFormat(fecha);
		Date date = null;
		try {
			date = sdf.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	//given a string 'pattern' sets the calendar
	public static Calendar asCalendar(String pattern, String fecha) {
		String[] fechaArray = fecha.split("-");

		int year = Integer.valueOf(fechaArray[0]);
		int month = Integer.valueOf(fechaArray[1]) - 1;
		int dayM = Integer.valueOf(fechaArray[2]);
		int dayW = Integer.valueOf(fechaArray[2]);
		
		Calendar c1 = Calendar.getInstance();
		c1.set(year, month, dayM);
		return c1;
	}
	//given a string 'pattern' sets the "month number year"
	public static String asString(String pattern, Date fecha) {
	
		String s = fecha.toString();
		return s;
	}


}
