package modulo6;
/**
 * @author David Tom�
 * M�dulo 6 ejercicio 1
 * Crear clase StringUtil dentro de paquete StringUtil con los metodos:
 * public static boolean containsDobleSpace(String str)
 * static boolean containsNumber(String str)
 */

public class Mod6_ejercicio1 {
	
	public static String cadena;
	
	public static void main(String[] args) {
		cadena = "hala madrid";
		System.out.println("La cadena tiene 2 espacios?"+containsDobleSpace(cadena));
		System.out.println("La cadena tiene numeros?"+containsNumber(cadena));
		cadena = "hala madrid por 13";
		System.out.println("La cadena2 tiene 2 espacios?"+containsDobleSpace(cadena));
		System.out.println("La cadena2 tiene numeros?"+containsNumber(cadena));
	}
	// funcion tipo boolean que retorna true si encuentra 2 espacios al analizar sus caracteres
	public static boolean containsDobleSpace(String str) {
		int doubleSpace = 0;
		for(int i=0;(i<str.length())&&(doubleSpace<2);i++) {
			if(str.charAt(i)==' ')	doubleSpace++;
		}
		if(doubleSpace==2)	return true;
		return false;
	}
	// funcion tipo boolean que retorna true si encuentra al menos 1 numero
	static boolean containsNumber(String str) {
		boolean hasNum = false;
		for(int i=0;(i<str.length())&&(hasNum==false);i++) {
			char c = str.charAt(i);
			if((c=='1')||(c=='2')||(c=='3')||(c=='4')||(c=='5')||
					(c=='6')||(c=='7')||(c=='8')||(c=='9'))	hasNum = true;
		}
		return hasNum;
	}

}
